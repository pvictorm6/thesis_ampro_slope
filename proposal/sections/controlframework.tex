%!TEX root = ../proposal.tex

\section{Human Inspired Control}
Based on the general scope of human-inspired walking, the general formulation starts considering a full robot model which fits the human dimensions for the purpose of gait generation. In particular this model considers a planar bipedal robot, since most stability problems come from the sagital plane. Based on human data acquired by IMUs, the human-inspired optimization \cite{AmesFirstSteps} generates a stable walking gait. 

\subsection{Robot Model}
Since we want the robot model to represent the human subject in the sagital plane, we consider a seven link planar robot, constructed from two feet, two shanks, two thighs and one torso. Figure \ref{fig:robotModel} shows the representation of the robot model, with the representation of the human leg (black) and the prosthetic leg (red).

\begin{figure}[h]
  \centering
    \includegraphics[scale=0.4]{figures/robotModel}
    \caption{Robot model inspired by human subject, including joint angles and human/prosthetic leg}
    \label{fig:robotModel}
\end{figure}

The coordinates of the robot state are also shown in Figure \ref{fig:robotModel} and are represented by the vector state $x = (\theta_{sa}, \theta_{sk}, \theta_{sh}, \theta_{nsh}, \theta_{nsk}, \theta_{nsa})^T$. The equation of motion of the continuous dynamics is given below:

\begin{align}
	D(\theta) \ddot{\theta} + H(\theta, \dot{\theta}) = Bu
	\label{eq:EOM}
\end{align}

where, $D(\theta) \in \mathbf{R}^{6x6}$ corresponds to the inertial matrix and $H(\theta, \dot{\theta}) \in \mathbf{R}^{6x1}$ contains the Coriolis effect $C(\theta, \dot{\theta}) \dot{\theta}$ and the gravity vector $G(\theta)$. Considering full actuation the torque map is the identity, $B = I_6$, and $u$ is the vector of torque inputs.
\newline
The system may be represented by an affine control system reformulating Eq. \ref{eq:EOM} into the following:

\begin{align}
	\dot{x} = f(x) + g(x)u
\end{align}

\subsection{Human inspired Outputs}
In order to obtain human-like walking, we impose the goal of driving the robot outputs $y^a(\theta)$ to the desired human outputs $y^d(t,\alpha)$ that corresponds to the canonical walking function (CWF) which is dependent on a set of parameters $\alpha$. Particularly for the model chosen with 7 links, we get 6 outputs of interest \cite{Wenlong}, therefore the human-inspired outputs can be represented as:

\begin{align}
	y(\theta, \dot{\theta}, \alpha) = \begin{bmatrix} y_1(\theta, \dot{\theta}, \alpha) \\ y_2(\theta, \alpha) \end{bmatrix} = \begin{bmatrix} y^a_1(\theta, \dot{\theta}) - v_{hip} \\ y^a_2(\theta) - y_2^d(\rho(\theta), \alpha) \end{bmatrix}
\end{align}

where $y_1(\theta, \dot{\theta})$ is the relative degree one output and $y_2(\theta, \alpha)$ contains the relative degree two outputs. Upon observation of human data, the linearized forward hip position $(\delta_{p_{hip}})$ was discovered to be dependent linearly on time, which motivates its usage as a phase variable for parameterization of the walking gait.

\begin{align}
	\rho({\theta}) = \frac{(\delta_{p_{hip}} - \delta_{p_{hip}}^+)}{v_{hip}}
	\label{eq:parameterizacion}
\end{align}

During an impact, the robot will be subjected to discontinuous changes in its states. In this case we need to impose the invariance of the relative degree two outputs, and this motivates the introduction of the Partial Hybrid Zero Dynamics (PHZD) constraints which yields a set of parameters $\alpha$. The Hybrid Zero Dynamics is defined as:

\begin{align}
	\mathbf{Z}_{\alpha} = \{ (\theta, \dot{\theta}) \in TQ_R : y_1(\theta, \dot{\theta}) = 0, y_2(\theta, \alpha) = 0, \\
	L_fy_2(\theta, \alpha) = 0 \}
\end{align}

Furthermore, the PHZD is defined as:

\begin{align}
	\mathbf{PZ}_{\alpha} = \{ (\theta, \dot{\theta}) \in TQ_R : y_2(\theta, \alpha) = 0, L_fy_2(\theta, \alpha) = 0 \}
\end{align}

The PHZD constraint, can now be easily expressed as:

\begin{align}
	\Delta_R(S_R \cap \mathbf{Z}_{\alpha}) = \mathbf{PZ}_\alpha
	\label{eq:PHZDconstraint}
\end{align}

	where $\Delta_R$ and $S_R$ are the reset map and swichting surface of the robot model \cite{Wenlong}.

\subsection{Human-Inspired Optimization}
Enforcing the PHZD constraint (\ref{eq:PHZDconstraint}), the human-inspired optimization yields trajectories which are provably stable and human-like. By enforcing more conditions related to the user preference and considering AMPRO II limitations on torque and velocities the optimization can yield directly implementable parameters without the need of further tunning. These specifications along with the PHZD constraint yields the following optimization problem:

\begin{align}
	\alpha^* = \underset{\alpha \in \mathbf{R}^{26} }{argmin} \hspace{1em} Cost_{HD}(\alpha) \\
	s.t. \hspace{2em} (PHZD) \\
	\textrm{Physical constraints}
\end{align}

The cost function and the implementation details are given in \cite{AmesFirstSteps}. The result of the optimization is the set of parameters, $\alpha^*$, which renders an optimal and provably stable human-like walking gait. In general, the optimization uses the trajectory of healthy subjects as the reference, then an optimal trajectory subject to the PHZD and Physical constraints, is generated.

\subsection{Canonical Walking Functions and Splines}
Since the time is parameterized by $\rho(\theta)$ in (\ref{eq:parameterizacion}), it is convenient to express the splines using this variable. In order to fully define the splines, the boundary conditions must be explicitly stated. As soon as the prosthetic foot lands on a arbitrary surface, the values of its position, velocity and acceleration can be used for the bounday conditions $P1$,  $DP1$, $D^2P1$. Considering that the objective function is the connection of this point to the CWF, we define the final boundary conditions in terms of the desired values of the relative degree two outputs.

\begin{align}
	P2 = y_2^d(\rho^*, \alpha) \\
	DP2 = \dot{y}_2^d(\rho^*, \alpha) \\
	DDP2 = \ddot{y}_2^d(\rho^*, \alpha)
\end{align}

where, $\rho^*$ is the designed convergence time which corresponds to the time when the splines smoothly intersects the CWF. Note that the evaluation of the spline will be computed from the following equation:

\begin{align}
	S(\rho(\theta)) = \begin{bmatrix} 1 & \rho(\theta)&  \rho(\theta)^2 & \rho(\theta)^3 \end{bmatrix} \lambda_{i(\rho(\theta))}
\end{align}

where, $\lambda_{i(\rho(\theta))}$ is the set of spline parameters corresponding to $\rho(\theta)$. The relative degree two output will depend on $\rho(\theta)$ since it can be based on the spline trajectory or the CWF and is therefore defined as:

\begin{align}
	y_2(\theta, \alpha) = 
	\begin{cases}
		y_2^a(\theta) - S(\rho(\theta)), \hspace{2.5em} if \hspace{1em} \rho(\theta) \leq \rho^* \\
		y_2^a(\theta) - y_2^d(\rho(\theta), \alpha), \hspace{1em} if \hspace{1em} \rho(\theta) \textgreater \rho^* 
	\end{cases}
\end{align}

Note that using the spline formulation, we lose the guarantees about stability and the PHZD constraint. However, since the human user is highly adaptable we assume that the user will remain stable considering the small changes introduced by the spline. Furthermore, the trajectories generated by the use of splines will be close to the ones generated by healthy human subjects. 