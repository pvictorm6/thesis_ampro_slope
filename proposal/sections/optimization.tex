%!TEX root = ../proposal.tex

\section{Optimization}
We need an algorithm that yields a smooth trajectory, the smoothness of the trajectory plays a key role in the robot performance, as non-smooth trajectories can cause large velocities or accelerations. However, more requirements are to be considered in generating the trajectory, the end point must be convergent with the nominal trajectory guaranteing smoothness in position, velocity and acceleration  as shown in Figure \ref{fig:formulationTraj}
\newline
A well-known solution to this problem is using a single cubic spline to connect the desired points. Cubic splines have four tunable parameters which can take care of continuity of position and velocity. However it lacks the capability to design the acceleration at the extreme points. The solution can be improved using several intermediate points (waypoints) connecting cubic splines between the origin and destination points. This technique is more advantageous as obstacle avoidance could be achieved by picking appropriate waypoints. Also acceleration could be chosen if an specific value is required. The usage of waypoints and a set of splines is represented in Figure \ref{fig:minimizeDistance}
\newline
We are interested in formulating a set of N cubic splines that can connect in a smooth way two trajectories based on a convex optimization. The fist conditions for the trajectory smoothness are addressed by imposing continuity in the extreme points, using the following:

\begin{align}
	S(t_s) = P1 \\
	S(t_c) = P2 \\
	\dot{S}(t_s) = DP1 \\
	\dot{S}(t_c) = DP2 \\
	\ddot{S}(t_s) = D^2P1 \\
	\ddot{S}(t_c) = D^2P2 
	\label{eq:ExtremePoints}
\end{align} 

\begin{figure}
  \centering
    \includegraphics[scale=0.35]{figures/formulationTraj}
    \caption{Two disconnected trajectories $C1$ and $C2$ can be connected trough a trajectory $S$, starting from time $t_s$ and finishing on $t_c$}
    \label{fig:formulationTraj}
\end{figure}

\begin{figure}
  \centering
    \includegraphics[scale=0.35]{figures/minimizeDistance}
    \caption{Connecting two different trajectories using a set of splines joined by specific waypoints}
    \label{fig:minimizeDistance}
\end{figure}

Position, velocity and acceleration must be smooth at the the waypoints. Each waypoint except the extreme points connects two splines. Both splines should have the same values at this waypoint connection as shown in Figure \ref{fig:splineSmooth} 

\begin{figure}[h]
  \centering
    \includegraphics[scale=0.4]{figures/splineSmooth}
    \caption{Cubic splines connected through waypoints}
    \label{fig:splineSmooth}
\end{figure}

The splines must meet the following:

\begin{align}
	S1(T(i)) = S2(T(i)) \\
	\dot{S1}(T(i)) = \dot{S2}(T(i)) \\
	\ddot{S1}(T(i)) = \ddot{S2}(T(i))
	\label{eq:InteriorPoints}
\end{align}

Since we want to generate a convergent trajectory, it is necessary to have a cost function based on the distance between the could trajectory and the generated splines, Figure \ref{fig:minimizeDistance} shows the distance between each waypoint to the desired trajectory. These distances will be used for the construction of the cost function minimized through a least square formulation:

\begin{align}
	min   \sum_i || S(T(i)) - C2(T(i)) || \\
	st   \hspace{2em} (1) - (\ref{eq:InteriorPoints})
\end{align}

Each cubic spline has the following form:

\begin{align}
	S_i = a_0^i + a_1^i (t-t_c) + a_2^i (t - t_c)^2 + a_3^i (t - t_c)^3
\end{align}

Which, after derivations yields:

\begin{align}
	\dot{S}_i = a_1^i + 2 a_2 ^i (t-t_c) + 3 a_3^i (t - t_c)^2 \\
	\ddot{S}_i = 2 a_2^i + 6 a_3^i (t - t_c)
\end{align}

Note that the splines $S_i$ and $S_{i+1}$ are continous and smooth in terms of position, velocity and acceleration. For position the following condition needs to be satisfied:

\begin{align}
	S_i(t_i) = a_0^i + a_1^i (\Delta_{t_i}) + a_2^i (\Delta_{t_i})^2 + a_3^i (\Delta_{t_i})^3 \\
	S_{i+1}(t_i) = a_0^{i+1} + a_1^{i+1} (\Delta_{t_i}) + a_2^{i+1} (\Delta_{t_i})^2 + a_3^{i+1}(\Delta_{t_i})^3 \\
	S_i(t_i) - S_{i+1}(t_i) = 0
\end{align}

Repeating the same procedure for velocity the following is obtained:

\begin{align}
	\dot{S}_i(t_i) = a_1^i + 2 a_2^i (\Delta_{t_i}) + 3 a_3^i (\Delta_{t_i})^2 \\
	\dot{S}_{i+1}(t_i) = a_1^{i+1} + 2 a_2^{i+1} (\Delta_{t_i}) + 3 a_3^{i+1}(\Delta_{t_i})^2 \\
	\dot{S}_i (t_i) - \dot{S}_{i+1}(t_i) = 0
\end{align}

Finally, considering acceleration, the following conditions needs to be satisfied:

\begin{align}
	\ddot{S}_i(t_i) = 2 a_2^i + 6 a_3^i (\Delta_{t_i}) \\
	\ddot{S}_{i+1}(t_i) = 2 a_2^{i+1} + 6 a_3^{i+1}(\Delta_{t_i}) \\
	\ddot{S}_i (t_i) - \ddot{S}_{i+1}(t_i) = 0	
\end{align}

These constraints can be written in a matrix form. Consider a vector $\lambda$ storing all the parameters of the splines, a segment of the vector $\lambda_{i,i+1}$ is constructed in the following way:

\begin{align}
	\lambda_{i,i+1} = \begin{bmatrix} a_0^i & a_1^i & a_2^i & a_3^i & a_0^{i+1} & a_1^{i+1} & a_2^{i+1} & a_3^{i+1} \end{bmatrix}^T
\end{align}

Then, the constraints can be written as follows:

\begin{align}
	\begin{bmatrix} 1 & \Delta_{t_i} & \Delta_{t_i}^2 & \Delta_{t_i}^3 & -1 & -\Delta_{t_i} & -\Delta_{t_i}^2 & -\Delta_{t_i}^3  \end{bmatrix} \lambda_{i,i+1} = 0 \\
	\begin{bmatrix} 0 & 1 & 2 \Delta_{t_i} & 3 \Delta_{t_i}^2 & 0 & -1 & - 2 \Delta_{t_i} & - 3 \Delta_{t_i}^2  \end{bmatrix} \lambda_{i,i+1} = 0 \\
	\begin{bmatrix} 0 & 0 & 2 & 6 \Delta_{t_i} & 0 & 0 & -2 & -6 \Delta_{t_i}  \end{bmatrix} \lambda_{i,i+1} = 0 
\end{align}

Considering the boundary conditions for the initial point, we have:

\begin{align}
	\begin{bmatrix} 1 & \Delta_{t_0} & \Delta_{t_0}^2 & \Delta_{t_0}^3 & 0 & 0 & 0 & 0 \end{bmatrix} \lambda_{0,1} = P1 \\
	\begin{bmatrix} 0 & 1 & 2 \Delta_{t_0} & 3 \Delta_{t_0}^2 & 0 & 0 & 0 & 0 \end{bmatrix} \lambda_{0,1} = DP1 \\
	\begin{bmatrix} 0 & 0 & 2 & 6 \Delta_{t_0} & 0 & 0 & 0 & 0 \end{bmatrix} \lambda_{0,1} = D^2P1
	\label{eq:matrixExtremeP}
\end{align}

Analogous formulation for the other extreme point, can be obtained in terms of $P2, DP2, D^2P2$
\newline
All the constraints can be written in a single matrix, which is represented by the constraint matrix $C$.

\begin{align}
	C \lambda = d
\end{align}

where, $d$ is the value that the equation must take in order to meet the constraint conditions, which can be $P1, DP1, D^2P1, P2, DP2, D^2P2$ or zero depending on the waypoint and the condition being enforced. Additionally, $\lambda$ is the collection of all the parameters, that is, $\lambda = \begin{bmatrix} \lambda_{0,1} & \lambda_{1,2} & ... & \lambda_{N-1,N} \end{bmatrix}$
\newline
In order to express the cost function, it is possible to obtain the value of each spline at its respective waypoint using the following equation:

\begin{align}
	S_i(t_i) = \begin{bmatrix} 1 & \Delta_{t_i} & \Delta_{t_i}^2 & \Delta_{t_i}^3 \end{bmatrix} \lambda_i = P \lambda_i
\end{align}

Then, all values at the waypoints are calculated in a vector using the following equation:

\begin{align}
	S = P \lambda
\end{align}

Finally, the minimization problem can be formulated as:

\begin{align}
	min || P \lambda - Y ||
	s.t \hspace{2em} C \lambda = d
\end{align}

where, $Y$ is the desired set of points to which we want to be close to. Note that the problem can be solved in real time applications. The analytic solution is provided in the appendix.
