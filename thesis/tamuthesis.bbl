\begin{thebibliography}{10}

\bibitem{AmesFS}
Aaron~D. Ames.
\newblock First steps toward automatically generating bipedal walking from
  human data.
\newblock {\em Robot Motion and Control 2011}, Springer London:89--116, 2012.

\bibitem{AmesHIC}
Aaron~D. Ames.
\newblock Human-inspired control of bipedal walking robots.
\newblock {\em IEEE Transactions on Automatic Control.}, 59(5), 2014.

\bibitem{cao1997constrained}
B~Cao, GI~Dodds, and GW~Irwin.
\newblock Constrained time-efficient and smooth cubic spline trajectory
  generation for industrial robots.
\newblock In {\em Control Theory and Applications, IEE Proceedings-}, volume
  144, pages 467--475. IET, 1997.

\bibitem{goebel2009hybrid}
Rafal Goebel, Ricardo~G Sanfelice, and Andrew Teel.
\newblock Hybrid dynamical systems.
\newblock {\em Control Systems, IEEE}, 29(2):28--93, 2009.

\bibitem{gregg2014towards}
Robert~D Gregg and Jonathon~W Sensinger.
\newblock Towards biomimetic virtual constraint control of a powered prosthetic
  leg.
\newblock {\em Control Systems Technology, IEEE Transactions on},
  22(1):246--254, 2014.

\bibitem{Atrias}
A~Hereid, S.~Kolathaya, M.S Jones, Van~Why J.W, and A.~D. Ames.
\newblock Dynamic multi-domain bipedal walking with atrias through slip based
  human-inspired control.
\newblock In ACM, editor, {\em Proceedings of the 17th International conference
  on Hybrid systems: computation and control.}, 2014.

\bibitem{hereid2015hybrid}
Ayonga Hereid, Christian~M Hubicki, Eric~A Cousineau, Jonathan~W Hurst, and
  Aaron~D Ames.
\newblock Hybrid zero dynamics based multiple shooting optimization with
  applications to robotic walking.
\newblock In {\em Robotics and Automation (ICRA), 2015 IEEE International
  Conference on}, pages 5734--5740. IEEE, 2015.

\bibitem{hogan1985impedance}
Neville Hogan.
\newblock Impedance control: An approach to manipulation: Part
  ii—implementation.
\newblock {\em Journal of dynamic systems, measurement, and control},
  107(1):8--16, 1985.

\bibitem{RoughTerrain}
Shishir Kolathaya and Aaron~D Ames.
\newblock Achieving bipedal locomotion on rough terrain through human-inspired
  control.
\newblock In {\em Safety, Security, and Rescue Robotics (SSRR), 2012 IEEE
  International Symposium on}, pages 1--6. IEEE, 2012.

\bibitem{CubicSpline}
J.~Zico Kolter and Andrew~Y. Ng.
\newblock Task-space trajectories via cubic spline optimization.
\newblock In {\em Robotics and Automation, 2009. ICRA '09. IEEE International
  Conference on}, pages 1675--1682, 2009.

\bibitem{lawson2014robotic}
Brian~Edward Lawson, J~Mitchell, Don Truex, Amanda Shultz, Elissa Ledoux, and
  Michael Goldfarb.
\newblock A robotic leg prosthesis: Design, control, and implementation.
\newblock {\em Robotics \& Automation Magazine, IEEE}, 21(4):70--81, 2014.

\bibitem{EffectsSlope}
Andrea~N. Lay, Chris~J. Hass, and Robert~J. Gregor.
\newblock The effects of sloped surfaces on locomotion: A kinematic and kinetic
  analysis.
\newblock {\em Journal of Biomechanics}, 39:1621--1628, 2006.

\bibitem{lemaire2000gait}
Edward~D Lemaire, David Nielen, and Marie~Andr{\'e}e Paquin.
\newblock Gait evaluation of a transfemoral prosthetic simulator.
\newblock {\em Archives of physical medicine and rehabilitation},
  81(6):840--843, 2000.

\bibitem{posturalAdaptation}
Alain Leroux, Joyce Fung, and Hugues Barbeau.
\newblock Postural adaptation to walking on inclined surfaces: I. normal
  strategies.
\newblock {\em Gait \& posture}, 15(1):64--74, 2002.

\bibitem{Inpatient}
Owings Maria~F. and Kozak Lola~Jean.
\newblock Ambulatory and inpatient procedures in the united states, 1996.
\newblock {\em National Center for Health Statistics.}, Vital Health(Stat
  13(139)), 1998.

\bibitem{InclinedDynamics}
Andrew~Stuart McIntosh, Karen~T. Beatty, Leanne~N. Dwan, and Deborah~R.
  Vickers.
\newblock Gait dynamics on an inclined walkway.
\newblock {\em Journal of Biomechanics.}, 39(13):2491--2502, 2006.

\bibitem{murray2005social}
Craig~D Murray.
\newblock The social meanings of prosthesis use.
\newblock {\em Journal of Health Psychology}, 10(3):425--441, 2005.

\bibitem{PoincareBook}
T.S. Parker and L.O. Chua.
\newblock {\em Practical numerical algorithms for chaotic systems}.
\newblock Springer New York, 1989.

\bibitem{SastryBook}
Shankar. Sastry.
\newblock {\em Nonlinear systems: analysis, stability, and control.},
  volume~10.
\newblock Springer Science \& Business Media, 2013.

\bibitem{SinnetHybridControl}
Ryan~W. Sinnet, Matthew~J. Powell, Rajiv~P. Shah, and Aaron~D. Ames.
\newblock A human-inspired hybrid control approach to bipedal robotic walking.
\newblock In {\em 18th IFAC World Congress}, 2011.

\bibitem{transfemoral}
Douglas~G. Smith.
\newblock The transfemoral amputation level, part 1.
\newblock URL:
  "http://www.amputee-coalition.org/resources/transfemoral-amputation-part-1/",
  Accessed: 03/01/2016.

\bibitem{GoldfarbDesign}
Frank Sup, Amit Bohara, and Michael Goldfarb.
\newblock Design and control of a powered transfemoral prosthesis.
\newblock {\em The International Journal of Robotics research.},
  27(2):263--273, 2008.

\bibitem{GoldfarbUpslope}
Frank Sup, Huseyin~Atakan Varol, and Michael Goldfarb.
\newblock Upslope walking with a powered knee and ankle prosthesis: initial
  results with an amputee subject.
\newblock {\em Neural Systems and Rehabilitation Engineering, IEEE Transactions
  on.}, 19(1):71--78, 2011.

\bibitem{AmpLevel}
RL~Waters, J~Perry, D~Antonelli, and H~Hislop.
\newblock Energy cost of walking of amputees: the influence of level of
  amputation.
\newblock {\em The Journal of bone and joint surgery.}, 1(58):42--46, 1976.

\bibitem{WendelPoincare}
Eric~DB Wendel and Aaron~D. Ames.
\newblock Rank properties of poincare maps for hybrid systems with applications
  to bipedal walking.
\newblock In ACM, editor, {\em Proceedings of the 13th ACM international
  conference on Hybrid systems: computation and control.}, 2010.

\bibitem{FeedbackControlBipedal}
E.~R Westervelt, J.~W. Grizzle, C~Chevallereau, J~Choi, and B.~Morris.
\newblock {\em Feedback control of dynamic bipedal robot locomotion.}
\newblock CRC press, 2007.

\bibitem{Westervelt}
E.R Westervelt, J.W Grizzle, and Koditscheck. D.E.
\newblock Hybrid zero dynamics of planar biped walkers.
\newblock {\em IEEE Transactions on Automatic Control}, 48(1):42--56, 2003.

\bibitem{Amber1}
Shishir~Nadubettu Yadukumar, Murali Pasupuleti, and Aaron~D. Ames.
\newblock From formal methods to algorithmic implementation of human inspired
  control on bipedal robots.
\newblock In Springer~Berlin Heidelberg, editor, {\em Algorithm Foundations of
  human inspired control on bipedal robots.}, pages 511--526, 2013.

\bibitem{AMPROIstairs}
H.~Zhao, J.~Reher, J.~Horn, V.~Paredes, and A.~D. Ames.
\newblock Realization of stair ascent and motion transitions on prostheses
  utilizing optimization-based control and intent recognition.
\newblock In IEEE, editor, {\em Rehabilitation Robotics (ICORR), 2015 IEEE
  International Conference on.}, 2015.

\bibitem{Amber2}
Hui-Hua Zhao, WL~Ma, Zeagler MB, and A.~D. Ames.
\newblock Human-inspired multi-contact locomotion with amber2.
\newblock {\em ACM/IEEE 5th International Conference on Cyber-Physical Systems
  (with CPS Week 2014)}, IEEE Computer Society, 2014.

\bibitem{zhaomulti}
Huihua Zhao, Jonathan Horn, Jacob Reher, Victor Paredes, and Aaron~D Ames.
\newblock Multi-contact locomotion on transfemoral prostheses via hybrid system
  models and optimization-based control.

\bibitem{ZhaoPrimitives}
Huihua Zhao, M.~J. Powell, and A.~D. Ames.
\newblock {\em Optimizal Control Applications and Methods}, chapter
  Human-inspired motion primitives and transitions for bipedal robotic
  locomotion in diverse terrain., pages 730--755.
\newblock 2014.

\bibitem{AMPRO1}
Huihua Zhao, Jake Reher, Jonathan Horn, Victor Paredes, and Aaron~D. Ames.
\newblock Realization of nonlinear real-time optimization based controllers on
  self-contained transfemoral prosthesis.
\newblock In ACM, editor, {\em Proceedings of the ACM/IEEE Sixth International
  Conference on Cyber-Physical Systems.}, 2015.

\end{thebibliography}
