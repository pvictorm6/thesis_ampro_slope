epstopdf AnkleTracking.eps
epstopdf AnkleTrackingTwoSteps.eps
epstopdf KneeTracking.eps
epstopdf KneeTrackingTwoSteps.eps

pdfcrop AnkleTracking.pdf
pdfcrop AnkleTrackingTwoSteps.pdf
pdfcrop KneeTracking.pdf
pdfcrop KneeTrackingTwoSteps.pdf

rm AnkleTracking.pdf
rm AnkleTrackingTwoSteps.pdf
rm KneeTracking.pdf
rm KneeTrackingTwoSteps.pdf

mv AnkleTracking-crop.pdf AnkleTracking.pdf
mv AnkleTrackingTwoSteps-crop.pdf AnkleTrackingTwoSteps.pdf
mv KneeTracking-crop.pdf KneeTracking.pdf
mv KneeTrackingTwoSteps-crop.pdf KneeTrackingTwoSteps.pdf

