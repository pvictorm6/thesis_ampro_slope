%!TEX root = ../tamuthesis.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  New template code for TAMU Theses and Dissertations starting Fall 2012.  
%  For more info about this template or the 
%  TAMU LaTeX User's Group, see http://www.howdy.me/.
%
%  Author: Wendy Lynn Turner 
%	 Version 1.0 
%  Last updated 8/5/2012
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                           SECTION I
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\pagestyle{plain} % No headers, just page numbers
\pagenumbering{arabic} % Arabic numerals
\setcounter{page}{1}


\chapter{\uppercase {Introduction}}
Amputation can play a negative role in the development of the persons in different contexts affecting the access to some opportunities and even impacting on the depression levels on the persons \cite{murray2005social}. In particular, lower limb amputation also imposes a big impact on the mobility, specifically walking gait. This motivates the development of prostheses that can attenuate this problem by imitating the behavior of normal human legs and providing the possibility of recovering the damaged walking skills. 
\newline
Lower limb amputations can be classified as amputations below the knee (transtibial) and above the knee (transfemoral). Experiments suggest that transfemoral amputees behave in a less active life style compared to transtibial amputees. Particularly the maximum aerobic capacity was significantly lower in transfemoral amputees compared to transtibial amputees \cite{AmpLevel}. These facts justify the study and development of transfemoral devices to bypass the limitations of transfemoral amputees. Transfemoral prostheses are expected to achieve a human-like walking and meet several requirements, such as bearing user's body weight, being safe on its operational cycle, cooperating with the user, providing possibility to traverse diverse terrains, etc.
\newline
It is estimated that there are approximately 185,000 new amputations each year in the United States \cite{Inpatient}. Furthermore, according to a report by the National Center for Health Statistics it is estimated that from the 1.2 million people in the United States that lives with limb loss, 18.5 percent are trans-femoral amputees \cite{transfemoral}. 
\newline
Despite the long history of feedback control, most of the available prostheses in the market are energetically-passive devices. In other words, they do not contribute to the insertion of net power during the walking cycle. Essentially they are mechanically-actuated devices with passive elements such as springs and dampers. Consequently, their mechanical characteristics are fixed and the range of terrains they can traverse is very limited. However, even with the exposed limitations of passive transfemoral prosthesis, they are reliable, lightweight and easy to use.
\newline
In recent years, several powered devices have been developed to overcome some of the weaknesses that passive prostheses have. These weaknesses include, for instance, the lack of user's control over the device and the limited adaptation to different kind of terrains. Basically a powered transfemoral device is composed by ankle and knee actuators driven by a computer implementing a feedback control algorithm designed to interact with the user and resemble the human walking gait for different scenarios. For example, the Ottobock's 3R60 EBS (Fig \ref{fig:Ottobock}) and the C-Leg4 (\ref{fig:cleg}) are commercially-available microcontroller-based knee devices that actuate only over the knee. The microcontrollers change knee damping properties depending on the selected task.
\newline
Powered transfemoral prosthesis with actuation in both the ankle and knee can offer the possibility to enhance the performance of the walking gait compared to the knee-actuated devices. Although it increases the complexity of the system, advances in feedback control can allow generation of human-like gaits with on-board processing.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=.40]{figures/Introduction/Ottobock3R60.jpg}
\caption{Ottobock 3R60 EBS knee joint with a prosthetic foot. This passive prosthesis is controlled through an hydraulic system adapted to the damping requirements of human walking.}
\label{fig:Ottobock}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=.40]{figures/Introduction/C-Leg.jpg}
\caption{Ottobock's C-Leg is a microprocessor controlled Knee. This powered prosthesis is controlled through an hydraulic system with controlled damping.}
\label{fig:cleg}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In the research community, one of the most representative fully-actuated transfemoral prosthesis was developed by Vanderbilt University \cite{lawson2014robotic,GoldfarbDesign}. This prosthesis is an electrically-actuated device controlled by an impedance control framework, where the impedance is modeled as a linear spring and a damper that change their properties along the gait cycle \cite{hogan1985impedance}. The gait cycle is decomposed into several phases along the gait cycles. Each phase is represented by a unique set of impedance parameters which are selected through a finite state machine. These parameters defining the impedance must be tuned offline based on joint sensor data, video recordings and user feedback \cite{GoldfarbDesign}. The Vanderbilt prosthesis is shown in Fig. \ref{fig:goldfarbD1}. Note that this framework depends on an offline tuning process to acquire the impedance parameters for each subject, which limits its practical implementation for a massive number of users. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=3]{figures/Introduction/goldfarbD1.jpg}
\caption{The Vanderbilt prosthesis is a powered transfemoral device. It was developed at the Center of Intelligent Mechatronics in the Vanderbilt University.}
\label{fig:goldfarbD1}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%since it allows to automatically generate stable and human-like walking gaits 
An important method that does not require a tuning process is the Human-Inspired Control (HIC) \cite{AmesHIC,AmesFS}. It is a control framework based on Hybrid Zero Dynamics (HZD) \cite{Westervelt,FeedbackControlBipedal} that was originally formulated for bipedal robotic walking. HZD combines discrete event (i.e., impact at the heel strike) and the continuous dynamics in the context of hybrid systems \cite{SinnetHybridControl}. HIC \cite{AmesFS, AmesHIC} generates stable and human-like walking gaits using human walking data and an HZD formulation. HIC the Human-Inspired Optimization (HIO) where human walking data is fitted by several lower-dimensional motion primitives. HIC has successfully been implemented in bipedal robots such as AMBER I \cite{Amber1}, AMBER II \cite{Amber2}, DURUS \cite{hereid2015hybrid} and ATRIAS \cite{Atrias}.

The HIC framework has been extended for powered transfemoral prosthesis and tested in the prosthetic device AMPRO I \cite{AMPRO1} (Fig. \ref{fig:AMPRO1}), the first generation of transfemoral prosthesis developed by AMBER Lab (now at Georgia Tech) at Texas A\&M University. Two different assumptions on the walking gait can be considered under the HIC framework: i) a flat foot and single-contact behavior that captures the minimum essentials of human walking \cite{AMPRO1}, and ii) a multi-contact behavior that expands the results on the flat foot assumption and that generates a more human-like walking gait \cite{zhaomulti}. The assumption of flat foot walking is useful for testing new algorithms since it provides a simple framework for implementation, after a successful implementation of an algorithm it can be translated into the multi-contact framework. It is reported that the knee trajectory in a flat foot framework was similar to real human knee trajectory, whereas the simulated ankle trajectory in a flat foot framework was significantly different from the real human ankle trajectory \cite{AMPRO1}. However, this deviation was mitigated by using multi-contact framework because multi-contact framework includes more phases of the human walking cycle \cite{zhaomulti}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=.40]{figures/Introduction/AMPRO1.png}
\caption{AMPRO 1 is a powered transfemoral prosthesis based on electrical motors and HIC.}
\label{fig:AMPRO1}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Upslope Walking}
As mentioned earlier, an important characteristic required for prosthesis is the capacity to traverse diverse terrains. To achieve this with a powered transfemoral prosthesis, it is important to construct controllers that can traverse common terrains in everyday life, such as rough terrain, stairs and ramps. Since flat ground and sloped surfaces are very common for most environments, this thesis focuses on the generation of walking gaits for flat ground and upslope surfaces. 
\newline
Even though many research efforts have been placed in flat ground walking \cite{gregg2014towards,AMPRO1,GoldfarbDesign}, the work of \cite{GoldfarbUpslope} extended the previous flat ground algorithm based on impedance control to inclined surfaces. The algorithm involves an offline tuning process to get the impedance parameters for two different inclinations of 5\degree\ and 10\degree. When an inclined surface is detected while walking, the closest inclination angle (either 5\degree\ or 10\degree) from the estimated inclination angle is determined and used for the impedance parameters. However, as the number of inclinations on the database increase, the tuning process becomes tedious and infeasible.
\newline
As indicated earlier, HIC can automatically generate stable and human-like walking gaits for transfemoral prosthesis, bypassing the tuning process. In the literature, HIC has been used for a ascending stair walking algorithm and implemented in AMPRO I \cite{AMPROIstairs}. This algorithm used $\mathit{a\ priori}$ information about the stairs profile (i.e., step height and width). Since HIC involves HIO, this algorithm cannot be computed online due to computational load required to solve the optimization.
\newline
Furthermore, information about the terrain cannot be obtained in advance for most situations, running HIO for many unknown surfaces and subjects beforehand seems impractical. This limitation of HIC motivates the usage of more flexible schemes. However, it is still beneficial to keep the advantages of HIC, specially the automatic generation of walking gaits.
\newline
To generate upslope walking trajectories in a prosthesis without the burden of running HIO for every possible slope, it is possible to use a convex optimization formulation since it has fast convergence to a solution that can be performed in real time. The generation of trajectories based on a convex optimization formulation has been utilized in legged robotics with the use of cubic splines \cite{CubicSpline}.

\section{Convex Optimization for Upslope Walking}
Since humans can walk on sloped surface without any problems, it would be beneficial to get insights from human upslope walking. That is, comparing joint kinematics during upslope walking and during flat ground walking may give us directions about qualitative strategies used by humans traversing this kind of terrains. The main idea is the generation of upslope walking trajectories by blending nominal flat ground walking gaits using the strategies found by observing human data. 
\newline
Considering biomechanical characteristics of walking on inclined surfaces, \cite{InclinedDynamics} reported that the knee seems to be sensitive enough to differentiate the surface inclination angle. Also, the knee flexion and ankle dorsiflexion increase as the surface slope increases. From these results, it can be deduced that the ankle and knee present some trends on its evolution with respect to the flat ground walking gait. These trends will be analyzed for the generation of strategies in the prosthetic device.



% specifically,



\section{AMPRO II}
AMPRO (A\&M Prosthetic) II is the second version of the AMPRO prostheses series that are custom-designed and built at Texas A\&M University by AMBER Lab (now at Georgia Tech). Compared to the first generation of AMPRO \cite{AMPRO1}, the main improvements of the second version are three folds. a) The weight is reduced to $5kg$, which is $3kg$ lighter than AMPRO I. b) The height is reduced by $71mm$ and the width is reduced by $36mm$. The smaller size allows a wider range of subject height. c) The motors and electronics are placed higher up on the calf, resulting a higher center of mass position, which yields a smaller centroidal moment of inertial for the user. Additionally, two FlexiForce force sensors are mounted on the heel and toe for contact detection. The main code structure including the online gait generation code is running on a low-powered single core micro computer, BeagleBone Black, at $200Hz$. Details of the design diagram can be seen in Fig. \ref{fig:AMPRO2}. In order to interface with AMPRO II, the human leg is instrumented with two IMUs located at the shin and thigh (Fig. \ref{fig:AMPRO2WOOLIM}).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=.3]{figures/Introduction/AMPRO2r.png}
\caption{AMPRO 2 is a second generation of the prosthetic devices at Texas A\&M. Each component is shown on one of the three views of the prosthesis.}
\label{fig:AMPRO2}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=.45]{figures/Introduction/Ampro2Woolim2.png}
\caption{IMU placement on the subject. The IMUs are used to interface with AMPRO II}
\label{fig:AMPRO2WOOLIM}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The HIC is implemented as a high level controller on the embedded BeagleBone Black, which takes as inputs the values from the encoders, the FlexiForce sensors and the IMUs data. The desired torques are commanded to the motor (ELMO) drivers through CAN communication and each motor (ELMO) driver implements a low-level joint controller to deliver the desired torque to their respective joints. To process the IMU data, another BeagleBone Black is used to compute an extended Kalman filter (EKF) which estimates the human hip position that is used as a parameterization of time for the human-inspired control. 


\section{Thesis Structure}
The flow of the thesis is as follows. In Chapter 2 the underlying kinematics for upslope walking is studied to characterize the strategies that healthy adults use when they are traversing inclined surfaces. By comparing ankle and knee trajectories from flat ground with upslope walking, it is possible to draw important conclusions about how humans blend the flat ground trajectories into upslope trajectories.
\newline
In Chapter 3, there is a presentation the basic definitions of the human walking and a summary of the HIC along with some discussion about its advantages and limitations in the context of upslope walking. 
\newline
The convex optimization formulation for the cubic splines is explained and constructed in Chapter 4. This formulation is based on the analysis of the human strategies for upslope walking. Particularly, it presents a convex optimization formulation to replicate the human strategies. This chapter also shows the modification of canonical walking functions to take consideration of the generated splines. 
\newline
The results of the application of the cubic splines with the HIC are shown in Chapter 5. The blended trajectories designed using the spline formulation are shown and explained for different scenarios. The implementation and data acquisition were conducted using the prosthetic device AMPRO II.
\newline
The conclusions are presented in Chapter 6. Additionally, a comparison is made between the standard walking controller for flat ground terrain and the spline-based controller. The idea is to motivate the usage of the spline-based controller as a general framework to traverse different surfaces within the same framework. Finally, open questions and potential improvements are discussed for guiding new research on this topic.