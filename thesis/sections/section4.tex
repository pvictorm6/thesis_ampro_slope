%!TEX root = ../tamuthesis.tex

\chapter{\uppercase{Optimization-based Spline generation for upslope walking}}
This chapter is motivated by the strategies found in human walking during upslope walking. A convex optimization based cubic spline problem is proposed due to its flexibility to be shaped into a desired behavior or trajectories and for the possibility to introduce constraints into the system that can be solved in real-time \cite{CubicSpline,cao1997constrained}. Furthermore, an analytical solution is proposed that can be solved easily with most linear algebra packages in the microcomputer used in the prosthesis.

\section{Problem Formulation}
The generation of a trajectory $S$ connecting a point (say, a point from trajectory $C1$) to a nominal trajectory (say, trajectory $C2$) as can be seen in Fig. \ref{fig:formulationTraj} is the main goal of this chapter.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.4]{figures/Optimization/formulationTraj}
    \caption{Two disconnected trajectories $C1$ and $C2$ can be connected trough a trajectory $S$, starting from time $t_s$ and finishing on $t_c$}
    \label{fig:formulationTraj}
\end{figure}

Additionally, some important constraints must be met such as continuity in the connection points with $C1$ and $C2$ and smoothness on the entire trajectory. A discontinuity in position would impose high velocities that can affect the performance of the walking gait, analogously a dicontinuity in velocities can impose large accelerations. 

A well-known solution to this problem is using a single cubic spline to connect the desired points. Cubic splines have four tunable parameters which can take care of continuity of position and velocity. Unfortunately it lacks the capability to provide smooth accelerations at the extreme points, also this single cubic spline cannot be shaped to follow a desired qualitative behavior (by fitting a trajectory). This solution can be improved using several intermediate points (waypoints) connecting cubic splines between the origin and destination points. This technique is more advantageous as continuity, smoothness and obstacle avoidance could be achieved by picking appropriate spline parameters \cite{CubicSpline}. The usage of waypoints and a set of splines (Fig. \ref{fig:minimizeDistance}) can be exploited to define a cost function that allows to shape the behavior of the set of cubic splines by minimizing the distance to some reference curve.

\begin{figure}[H]
  \centering
    \includegraphics[scale=0.4]{figures/Optimization/minimizeDistance}
    \caption{Connecting two different trajectories using a set of splines joined by specific waypoints}
    \label{fig:minimizeDistance}
\end{figure}

To guarantee that position, velocity and acceleration are smooth, it is necessary to check the waypoints. Each waypoint (except the extreme points) connects two splines. Both connected splines should have the same values in position, velocity and acceleration at each waypoint (Fig. \ref{fig:splineSmooth}).
\begin{figure}[H]
  \centering
    \includegraphics[scale=0.5]{figures/Optimization/splineSmooth}
    \caption{Cubic splines connected through waypoints}
    \label{fig:splineSmooth}
\end{figure}

Assuming that the set of N splines $S_1, S_2, ..., S_N$ is represented by $S$, which is defined appropriately such that for a corresponding time $t$ it takes the value of the corresponding spline $S_k(t), k \in [1,N]$. Particularly at the waypoints the time is defined by $T(i)$ for $i \in [1,N]$. The mathematical representation for smoothness in position, velocity and acceleration can be written as:
\begin{align}
\begin{split}
	S_1(T(i)) = S_2(T(i)) \\
	\dot{S_1}(T(i)) = \dot{S_2}(T(i)) \\
	\ddot{S_1}(T(i)) = \ddot{S_2}(T(i))
	\label{eq:InteriorPoints}
\end{split}
\end{align} 

Since the objective is to generate a convergent trajectory that follows a desired behavior, it is necessary to have a cost function based on the distance between a reference trajectory and the generated splines, Fig. \ref{fig:minimizeDistance} shows the distance between each waypoint to the desired trajectory. These distances will be used for the construction of the cost function, which is minimized through a least square formulation:
\begin{align}
	min   \sum_i || S(T(i)) - C2(T(i)) || \\
	s.t.   \hspace{2em} (\ref{eq:InteriorPoints}) \nonumber \\
	\text{(Smoothness constraints)} \nonumber
\end{align}
In order to describe the smoothness conditions, assume that each cubic spline has the following form:
\begin{align}
	S_i = a_0^i + a_1^i (t-t_c) + a_2^i (t - t_c)^2 + a_3^i (t - t_c)^3
\end{align}
where, $\lambda_i = \{a^i_0, a^i_1, a^i_2, a^i_3 \}$ is the set of parameters that defines the spline, note that $t_c$ is the convergence time (Fig. \ref{fig:formulationTraj}) and $t_s$ is the switching time. To include conditions of smoothness for velocity and acceleration, it is necessary to calculate the derivatives and double derivatives of the cubic spline.
\begin{align}
	\dot{S}_i = a_1^i + 2 a_2 ^i (t-t_c) + 3 a_3^i (t - t_c)^2 \\
	\ddot{S}_i = 2 a_2^i + 6 a_3^i (t - t_c)
\end{align}

To make the splines $S_i$ and $S_{i+1}$ to be continuous and smooth in terms of position, velocity and acceleration, its values at the waypoints should be equal. In the case of position, the conditions of smoothness are expressed as:
\begin{align}
	S_i(t_i) = a_0^i + a_1^i (\Delta_{t_i}) + a_2^i (\Delta_{t_i})^2 + a_3^i (\Delta_{t_i})^3 \\
	S_{i+1}(t_i) = a_0^{i+1} + a_1^{i+1} (\Delta_{t_i}) + a_2^{i+1} (\Delta_{t_i})^2 + a_3^{i+1}(\Delta_{t_i})^3 \\
	S_i(t_i) - S_{i+1}(t_i) = 0
	\label{eq:con_pos}
\end{align}

Considering smoothness in velocity, the conditions are expressed as:
\begin{align}
	\dot{S}_i(t_i) = a_1^i + 2 a_2^i (\Delta_{t_i}) + 3 a_3^i (\Delta_{t_i})^2 \\
	\dot{S}_{i+1}(t_i) = a_1^{i+1} + 2 a_2^{i+1} (\Delta_{t_i}) + 3 a_3^{i+1}(\Delta_{t_i})^2 \\
	\dot{S}_i (t_i) - \dot{S}_{i+1}(t_i) = 0
	\label{eq:con_vel}
\end{align}

Analogously for the smoothness conditions in acceleration:
\begin{align}
	\ddot{S}_i(t_i) = 2 a_2^i + 6 a_3^i (\Delta_{t_i}) \\
	\ddot{S}_{i+1}(t_i) = 2 a_2^{i+1} + 6 a_3^{i+1}(\Delta_{t_i}) \\
	\ddot{S}_i (t_i) - \ddot{S}_{i+1}(t_i) = 0	
	\label{eq:con_acc}
\end{align}

These constraints can be written in matricial form. Consider a vector $\lambda$ storing all the parameters of the splines, a segment of the vector $\lambda_{i,i+1}$ is constructed in the following way:
\begin{align}
	\lambda_{i,i+1} = \begin{bmatrix} a_0^i & a_1^i & a_2^i & a_3^i & a_0^{i+1} & a_1^{i+1} & a_2^{i+1} & a_3^{i+1} \end{bmatrix}^T
\end{align}

Then, the constraints (\ref{eq:con_pos}), (\ref{eq:con_vel}) and (\ref{eq:con_acc}) can be written as:
\begin{align}
	\begin{bmatrix} 1 & \Delta_{t_i} & \Delta_{t_i}^2 & \Delta_{t_i}^3 & -1 & -\Delta_{t_i} & -\Delta_{t_i}^2 & -\Delta_{t_i}^3  \end{bmatrix} \lambda_{i,i+1} = 0 \\
	\begin{bmatrix} 0 & 1 & 2 \Delta_{t_i} & 3 \Delta_{t_i}^2 & 0 & -1 & - 2 \Delta_{t_i} & - 3 \Delta_{t_i}^2  \end{bmatrix} \lambda_{i,i+1} = 0 \\
	\begin{bmatrix} 0 & 0 & 2 & 6 \Delta_{t_i} & 0 & 0 & -2 & -6 \Delta_{t_i}  \end{bmatrix} \lambda_{i,i+1} = 0 
\end{align}

The initial point continuity conditions ($P1, DP1, D^2P1$) are expressed using the first parameters of $\lambda$, using the following expression:
\begin{align}
	\begin{bmatrix} 1 & \Delta_{t_0} & \Delta_{t_0}^2 & \Delta_{t_0}^3 & 0 & 0 & 0 & 0 \end{bmatrix} \lambda_{0,1} = P1 \\
	\begin{bmatrix} 0 & 1 & 2 \Delta_{t_0} & 3 \Delta_{t_0}^2 & 0 & 0 & 0 & 0 \end{bmatrix} \lambda_{0,1} = DP1 \\
	\begin{bmatrix} 0 & 0 & 2 & 6 \Delta_{t_0} & 0 & 0 & 0 & 0 \end{bmatrix} \lambda_{0,1} = D^2P1
	\label{eq:matrixExtremeP}
\end{align}

Analogous formulation for the final point in its position, velocity and acceleration values $P2, DP2, D^2P2$ can be constructed. 
\begin{align}
	\begin{bmatrix} 0 & 0 & 0 & 0 & 1 & \Delta_{t_N} & \Delta_{t_N}^2 & \Delta_{t_N}^3  \end{bmatrix} \lambda_{N-1,N} = P2 \\
	\begin{bmatrix} 0 & 0 & 0 & 0 & 0 & 1 & 2 \Delta_{t_N} & 3 \Delta_{t_N}^2  \end{bmatrix} \lambda_{N-1,N} = DP2 \\
	\begin{bmatrix} 0 & 0 & 0 & 0 & 0 & 0 & 2 & 6 \Delta_{t_N}  \end{bmatrix} \lambda_{N-1,N} = D^2P2
	\label{eq:matrixExtremeP}
\end{align}

All the constraints can be written in a single matrix, which is represented by the constraint matrix $C$.
\begin{align}
	C \lambda = d
\end{align}
where, $d$ is the value that the equation must take in order to meet the constraint conditions, which can be $P1, DP1, D^2P1, P2, DP2, D^2P2$ or zero depending on the waypoint and the condition being enforced. Recall that $\lambda$ is the collection of all the parameters, that is, $\lambda = \begin{bmatrix} \lambda_{0} & \lambda_{1} & ... & \lambda_{N} \end{bmatrix}$
\newline
The cost function is constructed evaluating the splines at each waypoint:
\begin{align}
	S_i(t_i) = \begin{bmatrix} 1 & \Delta_{t_i} & \Delta_{t_i}^2 & \Delta_{t_i}^3 \end{bmatrix} \lambda_i = P_i \lambda_i
\end{align}

Then, all the values of the splines at the waypoints can be calculated in vector form using the following equation:
\begin{align}
	S = Diag^{N}_{i=0}[P_i] \lambda = P \lambda
\end{align}

Finally, the minimization problem can be formulated as:
\begin{align}
	min || P \lambda - Y || \\
	s.t \hspace{2em} C \lambda = d
\end{align}
where, $Y$ is the desired set of points describing the reference trajectory, in particular for the thesis $Y=C2$. Note that the problem can be solved in real time applications given its convex form. Moreover, an analytic solution can be obtained that can be implemented on the microcomputer of the prosthetic device AMPRO II.

\section{Analytical Solution}
Considering the problem expressed in the optimization:
\begin{align}
	min ||P \lambda - Y || \\
	s.t. \hspace{2em} C \lambda = d \nonumber
\end{align}

It is possible to rewrite the optimization problem as:
\begin{align}
	min || P \lambda - Y || = min \hspace{1em} \lambda^T P^T P \lambda - 2 Y^T P \lambda + Y^T Y \\
	s.t. \hspace{2em} C \lambda = d \nonumber
	\label{eq:minexp}
\end{align}

Then, the lagrangian of the optimization problem can be expressed as:
\begin{align}
	L = \lambda^T P^T P \lambda - 2 Y^T P \lambda + Y^T Y + \nu^T (C \lambda - d)
\end{align}

Using the KKT conditions for optimality:
\begin{align}
	2 P^T P \lambda - 2 P^T Y + C^T \nu = 0 \\
	C \lambda = d
\end{align}

It represents a matricial linear equation on $(\lambda, \nu)$ that can be expressed as:
\begin{align}
	\begin{pmatrix} 2 P^T P & C^T \\ C & 0 \end{pmatrix}  \begin{pmatrix} \lambda \\ \nu \end{pmatrix} = \begin{pmatrix} 2 P^T Y \\ d \end{pmatrix}
	\label{eq:ansolution}
\end{align}

This equation can be solved for $\lambda$ and $\nu$ easily using Eigen Libraries in C++, which can be implemented in real time and fits our requirements of implementation. An Eigen-based library has been created for the generation of a family of splines satisfying the conditions of smoothness and continuity. The public repository is located in:
\url{bitbucket.org/pvictorm6/splines_support}


\section{Human-Inspired Walking with Cubic Splines}
The first connection between the set of cubic splines and the canonical walking functions is given by their parameterization in time. However, as mentioned earlier, a replacement of time is given by $\rho(\theta)$ in (\ref{eq:time_parameterization}) which will be adopted by the splines as well. To fully define the domain of the splines, the boundary conditions must be explicitly stated, the splines will take action while $\rho(\theta) \in [t_s, t_s]$, otherwise, the canonical walking functions for flat ground walking will be used. 

As soon as the prosthetic foot lands on a arbitrary surface, the values of its position, velocity and acceleration can be used for the boundary conditions of the initial point ($P1$,  $DP1$, $D^2P1$). Considering that the objective function is the connection of this point to its respective flat ground CWF, the final boundary conditions ($P2$,  $DP2$, $D^2P2$) are defined in terms of the desired values of the relative degree two canonical walking functions.
\begin{align}
	P2 = y_2^d(\rho^*, \alpha) \\
	DP2 = \dot{y}_2^d(\rho^*, \alpha) \\
	D^2P2 = \ddot{y}_2^d(\rho^*, \alpha)
\end{align}
where, $\rho^* = t_c + t_s$ is the designed convergence time which corresponds to the time when the splines smoothly intersects the CWF. Note that the evaluation of the spline will be computed from the following equation:
\begin{align}
	S(\rho(\theta)) = \begin{bmatrix} 1 & \rho(\theta)&  \rho(\theta)^2 & \rho(\theta)^3 \end{bmatrix} \lambda_{i(\rho(\theta))}
\end{align}
where, $\lambda_{i(\rho(\theta))}$ is the set of spline parameters corresponding to $\rho(\theta)$. The relative degree two outputs will depend on the spline trajectory or the CWF. Therefore, the outputs can be expressed as:
\begin{align}
	y_2(\theta, \alpha) = 
	\begin{cases}
		y_2^a(\theta) - S(\rho(\theta)), \hspace{2.5em} if \hspace{1em} \rho(\theta) \leq \rho^* \\
		y_2^a(\theta) - y_2^d(\rho(\theta), \alpha), \hspace{1em} if \hspace{1em} \rho(\theta) \textgreater \rho^* 
	\end{cases}
\end{align}

Note that using the spline formulation, we lose the guarantees about stability because the splines trajectories will evolve outside the PHZD surface. However, since the human inherent controller is highly adaptable and robust we assume that the user will remain stable considering the small changes introduced by the spline. Furthermore, the trajectories generated by the use of splines will converge to the provably stable CWF. 

\subsection{Generated Trajectories}
Using the optimization problem proposed, a simulation in MATLAB was performed to obtain the resulting trajectories for ankle and knee under a different initial and final condition (\ref{fig:SplineSim}) based on the designed flat ground trajectories (Fig. \ref{fig:KneeAnkleDesign}). The optimization was solved using the analytical solution shown in (\ref{eq:ansolution}). Note that both extremes are generated using the spline formulation for the estimation of the behavior of the joints. However, the implementation of the framework dictates that the first part the prosthetic standing phase is generated using the spline trajectory generation and the last part of the prosthetic swing phase is generated through the use of low gain PD control. The results achieved using the analytic form of the problem yielded the same results as using the quadratic program solver built-in in MATLAB. This results were also tested in C++ for a series of different conditions to ensure that the solutions are valid.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
\centering
\includegraphics[scale=0.22]{figures/Optimization/SplineFigures/Ankle.pdf}
\includegraphics[scale=0.22]{figures/Optimization/SplineFigures/Knee.pdf}
\caption{Trajectory generation for ankle and knee joint. The splines has been generated in two extremes to show the effect of the splines. }
\label{fig:SplineSim}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Implementation Details}
The Table \ref{tab:implementation} shows the parameters used to generate the splines and activate the impedance control, note that the parameterized time indicates 100\% of step progression. The low gain PD control is used on the final part of the prosthetic swing phase and is aimed to grant terrain adaptation and provide the initial conditions to the spline formulation. The controller can be expressed as:
\begin{align}
	u = K_p(\theta^i_d - \theta^i_a) + K_d(\dot{\theta}^i_d - \dot{\theta}^i_a), \hspace{1em} i \in \{ankle, knee\}
\end{align}


\begin{table}[]
\centering
\caption{Implementation parameters}
\label{tab:implementation}
\begin{tabular}{|l|l|l|}
\hline
\textbf{Parameter}        & \textbf{Value} & \textbf{Unit} \\ \hline
\textit{Switching time}   & 0              & Unitless      \\ \hline
\textit{Convergence time} & 0.2            & Unitless      \\ \hline
\textit{Impedance time}   & 0.6            & Unitless      \\ \hline
\textit{Parameterized time (max)}     & 0.823          & Unitless      \\ \hline
\end{tabular}
\end{table}