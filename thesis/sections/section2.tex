%!TEX root = ../tamuthesis.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  New template code for TAMU Theses and Dissertations starting Fall 2012.  
%  For more info about this template or the 
%  TAMU LaTeX User's Group, see http://www.howdy.me/.
%
%  Author: Wendy Lynn Turner 
%	 Version 1.0 
%  Last updated 8/5/2012
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                           SECTION II
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{\uppercase {Human upslope walking analysis}}
This chapter presents an overview of the human data analysis for flat ground and upslope walking gaits. In particular, by analyzing the kinematic data of healthy humans during upslope walking it is possible to extract high level strategies that motivate the main formulation: blending flat ground trajectories into upslope walking gaits.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Bipedal Locomotion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Bipedal locomotion is a highly non-linear process, since several muscles can contribute during a single movement of a limb, the analysis of the dynamics and forces involved during human walking can be complex. Given the difference in dynamics between the human leg and a transfemoral prosthesis, it is not possible to find a simple correspondence between forces, instead a kinematic relation can be used to capture the high-level human behavior.

\subsection{Kinematics of Human Walking}
Human walking can be studied in the light of the kinematics despite the differences in dynamics between a robotic model and an actual human. To study kinematics of human walking it is important to distinguish between the basic walking phases that are involved in the walking cycle: \emph{single support phase} and \emph{double support phase}. During single support phase (Fig. \ref{fig:WalkingPhases}) the leg supporting the human body weight is called \emph{stance leg} and the leg going forward is the \emph{swing leg}. Furthermore, in the context of prostheses, it is useful to differentiate between the human leg and the prosthetic leg, for instance, it is useful to use human stance leg or prosthesis stance leg to differentiate between possibilities.
\newline
To motivate the previous definitions, a description of normal walking cycle will be discussed. During walking, there is a cyclic process on the movement of the legs: starting in double support phase (Fig. \ref{fig:WalkingPhases}) both legs are supporting the human body weight, then one of the legs become stance leg and the other swing leg. The swing legs moves to its desired location until it impacts the ground, entering again to the double support phase, swapping the naming of the legs (stance leg and swing leg) and finalizing one step. Note that for the scope of the thesis, flat foot walking is being imposed on the walking gait and behaviors such as heel strike or toe push are left for future work.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=1]{figures/CWF/WalkingPhases.png}
\caption{Illustration of the phases of human walking under the assumption of flat foot walking. During double support phase both legs supports the weight contrary to single support phase where one the legs support the weight.}
\label{fig:WalkingPhases}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Also, the knee and ankle movements can be described by their direction of movement, particularly flexion and extension plays an important role to identify clearly the direction of movement of a joint. For ankle, dorsiflexion happens when the foot rotates around the ankle toward the leg and plantarflexion otherwise (Fig. \ref{fig:AnkleKneeMovements}). In the case of the knee, extension happens when this rotation makes the leg move forward and Knee flexion otherwise (Fig. \ref{fig:AnkleKneeMovements}). 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{figures/CWF/AnkleKneeMovements.png}
\caption{The ankle joint can move in two directions, to avoid ambiguity those directions are called dorsiflexion and plantar flexion. Regarding the knee the directions of rotation are called as flexion and extension.}
\label{fig:AnkleKneeMovements}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This subsection has described important terminology to reference rotational movements of the lower limbs that will be useful for future references in the thesis. The next subsection will explore the kinematics involving upslope walking in healthy humans. 	 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Upslope Walking Kinematics}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Inclined surfaces are often present during locomotion activities, pedestrians walk on this kind of surface frequently in the streets, buildings and other environments. In consequence upslope walking is an important terrain that needs to be traversed by persons using transfemoral prostheses. As a first step for the design of upslope walking gaits, looking at human data would allow to collect important conclusions.
\newline
A great amount of study has focused primarily on flat ground walking \cite{InclinedDynamics}, studying the changes in the kinematics pattern with respect to flat ground walking when humans perform upslope walking may expose some inherent behavior that can provide insight into the control strategies used \cite{EffectsSlope} by humans to traverse inclined surfaces. Those strategies could be captured and implemented into prosthetic devices in order to generate upslope walking gaits from a suitable nominal flat ground walking gait.  

\section{Human Walking Data}
Motivated by the studies in inclined surface walking \cite{InclinedDynamics,EffectsSlope,posturalAdaptation}, it is shown that the knee flexion increases as the inclination of the surface increases. In particular the knee trajectory for different surfaces (Fig. \ref{fig:HumanKnee}) indicated a consistent trend on the trajectory changes with respect to slope increase. The same trend, although with less variation, is present in the ankle trajectories (Fig. \ref{fig:HumanAnkle}). This increase in the initial and final points for the trajectories for both joints are explained partially on the geometry of the surface because it makes the ankle dorsiflexion and knee flexion to increase in order to avoid foot scuffing with the surface.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{figures/humandata/humandata/Ankle.pdf}
\caption{Ankle Trajectory for 0\degree, 5\degree, 8\degree and 10\degree slope inclinations. The data is taken from McIntosh \cite{InclinedDynamics}.}
\label{fig:HumanAnkle}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{figures/humandata/humandata/Knee.pdf}
\caption{Knee Trajectory for 0\degree, 5\degree, 8\degree and 10\degree slope inclinations. The data is taken from McIntosh \cite{InclinedDynamics}.}
\label{fig:HumanKnee}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Strategies for Walking Upslope}
The trajectories for ankle and knee (Fig. \ref{fig:HumanAnkle} and Fig. \ref{fig:HumanKnee}) shows that most of the variation on trajectories is present in the extremes of the trajectories, in other words, at the beginning of stance phase and at the final part of the swing phase. In the next subsections a brief discussion about the human strategies used during upslope walking will be used to motivate the blending of flat ground walking into upslope walking in the context of a transfemoral prosthesis.

\subsection{Human Strategy}
Although that a proper analysis of upslope walking should involve the dynamics \cite{InclinedDynamics}, focusing on the kinematic trends allows to develop high-level strategies to 'mimic' the behavior of humans during upslope walking independently of the dynamics differences between a human leg and the prosthetic leg. The first values of ankle and knee (Fig. \ref{fig:HumanAnkle} and Fig. \ref{fig:HumanKnee}) are imposed by the inclined surface, in other words, the geometry of the slope modifies the angular values of the ankle and knee. As the slope increases there is an increase in knee flexion and ankle dorsiflexion. Those results are consistent with \cite{InclinedDynamics}, which reports that knee flexion at heel strike increase from 7\degree\ from 33\degree\ with an increase of surface inclination angle from 0\degree\ to +10\degree. Note that knee is the most sensitive joint with respect to the slope inclination \cite{InclinedDynamics}.
\newline
The differences in ankle and knee introduces with respect to the flat ground trajectories can be considered as a new initial condition into the system, it is observed that from this initial condition the trajectories converge into the nominal flat ground trajectories approximately at 40\% of the step progression and evolves similarly to the flat ground walking gait until approximately 80\% of the step progression, where the trajectories suffer deviations again because of the geometry of the surface.


\subsection{Controller Strategy}
A simple algorithm is proposed to blend flat ground into upslope walking trajectories. This algorithm is based in two simple actions: (1) generate in real time a trajectory that starts from the condition imposed by the surface (red shade region in Fig. \ref{fig:Strategy}) and (2) use low gain PD control that can offer terrain adaptation at the final part of the step (blue shaded region in Fig. \ref{fig:Strategy}). The low gain PD control can shape the angles of the joints at the final part of the step progression in order to let the prosthesis to acquire more knee flexion as the slope increases (Fig. \ref{fig:adaptation}).
\newline
In particular, as the generation of trajectories requires to be calculated in real time and to meet several constraints, such as smoothness and boundedness for physically admissible velocities and accelerations, the formulation takes the form of a convex optimization problem. The convex optimization formulation will be discussed later and merged into the human-inspired walking formulation to generate a complete framework that yields flat ground and upslope walking gaits. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=0.6]{figures/LowPD/Low_gain_PD_action2.JPG}
\caption{Low gain PD action used to enable terrain adaptation.}
\label{fig:adaptation}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{figures/humandata/humandata/Strategies.png}
\caption{Ankle and knee flat ground trajectories. The red shaded region represents the action of the splines and the blue shaded regions the action of the low PD control. The human data is taken from McIntosh \cite{InclinedDynamics}.}
\label{fig:Strategy}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The low gain PD control on the ankle and knee act as a passive spring and damper with a fixed equilibrium, the parameters are chosen in this case to adapt to knee surfaces. The controller takes the following form:
\begin{align}
	\tau_{i} = kp_{i} (\theta_{actual,i} - \theta_{desired,i}) + kd_i (\dot{\theta}_{actual,i} - \dot{\theta}_{desired,i}) , \hspace{2em} i \in \{ankle, knee\}
\end{align}

where, $k_i$, $\theta_i$ and $b_i$ are the stiffness, equilibrium point and damping parameters describing the impedance. Note that impedance control is implemented in the last part of the step progression, most of the time the human-inspired control will drive the joints.








