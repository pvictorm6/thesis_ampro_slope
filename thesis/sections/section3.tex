%!TEX root = ../tamuthesis.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  New template code for TAMU Theses and Dissertations starting Fall 2012.  
%  For more info about this template or the 
%  TAMU LaTeX User's Group, see http://www.howdy.me/.
%
%  Author: Wendy Lynn Turner 
%	 Version 1.0 
%  Last updated 8/5/2012
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                           SECTION III
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\chapter{\uppercase{Flat terrain gait generation}}
The generation of upslope walking gaits starts from the construction of automatically stable and human-like flat ground trajectories using the human-inspired control approach.
The HIC \cite{AmesHIC,AmesFS,Amber1} is a method based on the HZD approach \cite{Westervelt,FeedbackControlBipedal} that uses human-data as a reference for the construction of functions that represent the human walking through simple mathematical expressions. These functions are fully defined through a non-linear optimization problem that is used to guarantee stability and to consider physical constraints of the device. By tracking these functions by an appropriate use of feedback control, the system becomes stable and performs human-like walking in bipedal robots and transfemoral prostheses. 

\section{Bipedal Model}
A planar bipedal robotic model with 7-link (one torso, two thighs, two calves and two feet) which shares similar anthropomorphic values as the human is construced to have a representation of the human in the generation of the walking gait. Analogously, the coordinates are chosen based on the human subject (\ref{fig:WoolimModel}) as $\theta = \begin{bmatrix} \theta_{sa} & \theta_{sk} & \theta_{sh} & \theta_{nsh} & \theta_{nsk} & \theta_{nsa} \end{bmatrix}^T \in Q_R$, where $Q_R$ is the configuration space, a submanifold of $\mathbb{R}^6$.
\newline
The walking gait will be generated with the additional assumption of flat foot walking. It is considered that flat foot walking captures the essential movements of the walking gait \cite{AMPRO1} and is used as the testbed for implementation on more human-like walking representation such as multi-contact walking (for modeling multi-contact behavior see \cite{Amber2}). Additionally, during human walking, the impact with the ground plays a relevant event on the system that changes instantly the joint velocities \cite{FeedbackControlBipedal}. In order to take in consideration this discrete event, the bipedal model is regarded as a hybrid system \cite{goebel2009hybrid,FeedbackControlBipedal} with continuous dynamics and a discrete event.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=0.35]{figures/CWF/ampro2model.JPG}
\caption{Bipedal robotic model considering the same anthropomorphic dimensions as the human subject.}
\label{fig:WoolimModel}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The equations of motion representing the continuous dynamics of the system can be expressed as:
\begin{align}
	D(\theta) \ddot{\theta} + H(\theta, \dot{\theta}) = Bu
	\label{eq:EOM}
\end{align}
	where, $D(\theta) \in \mathbb{R}^6$ is the inertial matrix and $H(\theta, \dot{\theta}) = C(\theta) \dot{\theta} + G(\theta)$, $C(\theta)$ the Coriolis term and $G(\theta)$ the gravity vector. Since we are assumming full actuation the torque map is the identity matrix, $B=I_6$. The input to the system is given by the torques $u$. Expressing (\ref{eq:EOM}) in terms of state space variable $x = (\theta, \dot{\theta})^T$, we found an affine control system representation as shown below:
\begin{align}
	\dot{x} = f(x) + g(x)u
	\label{eq:controlsystem}
\end{align}
	where,
\begin{align}
	f(x) = \begin{bmatrix} \dot{\theta} \\ -D^{-1}(\theta) H(\theta, \dot{\theta}) \end{bmatrix} \\
	g(x) = \begin{bmatrix} 0 \\ D^{-1}(\theta) B(\theta) \end{bmatrix}
\end{align}

\theoremstyle{definition}
\begin{definition}%{Hybrid control system}
A hybrid control system is a tuple:
\begin{align}
	\mathcal{HC} = (X, U, S, \Delta, f, g)
	\label{eq:hcs}
\end{align}
where,
\begin{itemize}
	\item $X$ is a smooth submanifold of $\mathbb{R}^6$.
	\item $U \subseteq \mathbb{R}^6$ is the space of admissible controls.
	\item $S \subsetneq X$ is the guard or switching surface.
	\item $\Delta: S \rightarrow X$ is the reset map.
	\item $(f,g)$ is a control system s.t $\dot{x}=f(x)+g(x)u$.
\end{itemize}
\end{definition}

To discriminate between single support phase and double support phase in the walking cycle, the unilateral constraint $h(\theta)$ is used. This constraint is the distance of the swing foot with respect to the ground, while this constraint is positive ($h(\theta) > 0$), it ensures that the swing foot is above the ground. An impact occurs when the unilateral constraint becomes zero ($h(\theta)=0$) and the velocity of impact is greater than zero ($\dot{h}(\theta) > 0$). Then the single support phase can be expressed as:
\begin{align}
	X = \{ (\theta, \dot{\theta}) \in TQ_R: h(\theta) \geq 0 \}
\end{align}
where, $TQ_R$ is the tangent bundle of the system. The \emph{guard} is defined as the boundary of the domain with the unilateral constraint decreasing, in other words, when the leg is impacting the ground with certain velocity. The guard can be expressed as:
\begin{align}
	S = \{  (\theta, \dot{\theta}) \in TQ_R: h(\theta) = 0, \frac{dh(\theta)}{dt} > 0  \}
	\label{eq:guard}
\end{align}
To model the discrete event (impact), we assume that the impacts are plastic with impulsive forces acting on the system leg at the moment of contact. It is required to consider the extended coordinates that include the position of the support foot given by $\theta_e = (p_x, p_z, \theta)^T \in \mathbb{R}^2 \times Q_R$. Assume that the position of the swing foot end is defined as $\Gamma(\theta_e)$ and its Jacobian is given by $J_{\Gamma}(\theta_e)$. After an impact occurs the velocity of the joints suffers an instantaneous change in velocity, described by the \emph{impact map}:
\begin{align}
	\dot{\theta_e}^{+} = P(\theta_e)  \dot{\theta_e}^{-} = (I - D^{-1}(\theta_e) J_\Gamma^T(\theta_e) (J_\Gamma(\theta_e) D^{-1}(\theta_e) J_\Gamma^T(\theta_e))^{-1} J_\Gamma(\theta_e)) \dot{\theta_e}^{-}
	\label{eq:impact}
\end{align} 
Note that the joint positions does not change after the impact \cite{FeedbackControlBipedal}.
\newline
Whenever impacts occur the legs must be swapped, i.e, the support leg must be relabeled as the swing leg and the swing leg must be relabeled as the support leg. This is possible to achieve by the use of a coordinate transformation $\Re$. This transformation is included in the \emph{reset map} $\Delta$.
\begin{align}
	\Delta: S \rightarrow X, \hspace{4em} \Delta(\theta, \dot{\theta}) = \begin{bmatrix} \Re && 0 \\ 0 && \Re \end{bmatrix} \begin{bmatrix} \theta \\ P(\theta) \dot{\theta} \end{bmatrix}
	\label{eq:discretemap}
\end{align}

In order to consider both the continuous dynamics and the discrete event the model was constructed as a hybrid control system (\ref{eq:hcs}).

\section{Human-Inspired Control (HIC)}
With the appropriate model of the bipedal robotic system correlated with the human user, the next step towards the control design is the the selection of control objectives. The idea is the tracking of functions that capture the behavior of human walking, these functions are the canonical walking functions, they are canonical in the sense that they are intrinsic in human walking. By tracking the canonical walking functions a robotic model can exhibit the same behavior as human walking despite the difference in dynamics \cite{AmesFS,SinnetHybridControl}.

\subsection{Canonical Walking Functions (CWF)}
The canonical walking functions are inherent to human walking, Ames \cite{AmesFS} conducted experiments with several subjects, they exhibited the same evolution of some specific joint combinations, called as canonical walking functions. Considering the 7-link bipedal robot and the flat foot assumption a total of 6 canonical walking functions are required (Fig. \ref{fig:cwf}), $ \delta p_{hip}$ the linearized hip position, $\theta_{sk}$ the supporting knee, $\theta_{nsk}$ the non-supporting knee, $\delta m_{nsl}$ the non-supporting slope, $\theta_{torso}$ the torso angle and $\theta_{nsf}$ the non-supporting foot.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{figures/CWF/matlab/CWF1.pdf}
\includegraphics[scale=0.25]{figures/CWF/matlab/CWF2.pdf}
\caption{Human data collected though IMUs represented as canonical walking functions. They are conformed by the hip position (m) and the joints relations (rad).}
\label{fig:cwf}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The canonical walking functions have two different types of outputs, one of them corresponds to the linearized velocity of the hip ($y^d_1(\theta, \dot{\theta})$) and the rest can be modeled as the solution of a second order system ($y^d_2(\theta, \dot{\theta})$). 
\begin{align}
y^d_1(\theta, \dot{\theta}) = \delta \dot{p}_{hip}(\theta, \dot{\theta}) \\
y^d_2(\theta, \dot{\theta}) = \begin{bmatrix} \theta_{sk} , \theta_{nsk} ,\delta m_{nsl} ,\theta_{tor} ,\theta_{nsf} \end{bmatrix}^T
\end{align}

In general, each output $y_d(t, \alpha)$ can be described as functions dependent on a set of parameters $\alpha$. Note that $v_{hip}$ is considered to be a member of $\alpha$
\begin{align}
	y^1_d(t) = v_{hip} t \\
	y^2_{d,i}(t, \alpha) = e^{-\alpha_4 t} ( \alpha_1 cos(\alpha_2 t) + \alpha_3 sin(\alpha_2 t)) + \alpha_5 
\end{align} 
where, $i \in \{\theta_{sk}, \theta_{sk}, \theta_{nsk}, \delta m_{nsl}, \theta_{torso}, \theta_{nsf} \}$. 

\subsection{Human-Inspired Outputs (HIO)}
The human inspired outputs are the tracking errors between the actual and desired canonical walking functions. By driving them to zero ($y(\theta, \dot{\theta}, \alpha) \rightarrow 0$), the robotic model will perform a human-like walking gait. 
\begin{align}
	y(\theta, \dot{\theta}, \alpha) = \begin{bmatrix} y_1(\theta, \dot{\theta}) \\ y_2(\theta, \alpha) \end{bmatrix} = \begin{bmatrix} y_1^a(\theta, \dot{\theta}) - v_{hip} \\ y_2^a(\theta) - y_2^d(\rho(\theta), \alpha) \end{bmatrix}
	\label{eq:hioutputs}
\end{align}
Note that $y_1(\theta, \dot{\theta})$ is a relative degree one output and $y_2(\theta, \alpha)$ corresponds to the relative degree two outputs \cite{SastryBook}. $\rho(\theta)$ is used as a parameterization of time based on the observation that the hip position is regarded to increase linearly on time \cite{AmesFS}. The parameterization is given as:
\begin{align}
	\rho(\theta) = \frac{\delta_{p_{hip}}(\theta) - \delta_{p_{hip}}^{+}}{v_{hip}}
	\label{eq:time_parameterization}
\end{align}

where, $\delta_{p_{hip}}^{+}(\theta)$ is the position of the hip at the beginning of the step, designed in the human-inspired optimization, described in the next section and $\delta_{p_{hip}}(\theta)$ is the actual (linearized) position of the hip. The importance of the parameterization of time is that using $\rho(\theta)$ as a phase variable the user have control over the progression of the step, independent of time. The interaction between the user and the prosthesis makes it specially well suited for transfemoral prosthesis. The calculation of the phase variable $\rho(\theta)$ depends on which leg is supporting the user's weight. If the user is standing on the prosthesis leg, then the encoders are used to calculate $\rho$, otherwise, the IMUs attached to the healthy leg are used to estimate through a EKF the actual hip position \cite{AMPRO1}.

\subsection{Control Design}
Considering the human-inspired outputs defined in (\ref{eq:hioutputs}) the idea is to drive these outputs to zero. Applying the feedback linearization control \cite{SastryBook} to the outputs, the resulting control law is:
\begin{align}
	\begin{bmatrix} \dot{y_1} \\ \ddot{y_2} \end{bmatrix} = \underbrace{\begin{bmatrix} L_{fy_1}(\theta, \dot{\theta}) \\ L_{fy_2}^2(\theta, \dot{\theta}, \alpha) \end{bmatrix}}_{L_f(\theta, \dot{\theta}, \alpha)} + \underbrace{\begin{bmatrix} L_{gy_1}(\theta, \dot{\theta}) \\ L_gL_{fy_2}(\theta, \dot{\theta}, \alpha) \end{bmatrix}}_{\mathcal{A}(\theta, \dot{\theta}, \alpha)}u
\end{align}

	where, $L_f(\theta, \dot{\theta}, \alpha)$ represents the lie derivatives and $\mathcal{A}(\theta, \dot{\theta}, \alpha)$ is the decoupling matrix, which is invertible due to the selection of the outputs \cite{ZhaoPrimitives,AmesFS}.
	\newline
Choosing a control a control input $u \in \mathcal{U}$ in the form $u = \mathcal{A}^{-1}(\theta, \dot{\theta}, \alpha) ( L_f(\theta, \dot{\theta}, \alpha) + \mu$, the equation (\ref{eq:hioutputs}) becomes:
\begin{align}
	\begin{bmatrix} \dot{y_1} \\ \ddot{y_2} \end{bmatrix} = \mu
\end{align}

Note that for the bipedal model, exact information about the dynamics can be obtained, this allows to simulate the system and observe the resulting walking gait. However, for implementation purposes the dynamics of the user are unknown, thus the implementation of the controller needs to be model independent such as PD control or Model Indepedent QP \cite{AMPRO1}.
\newline
To drive the outputs to zero the form that $\mu$ take can be designed as:
\begin{align}
	\mu = \begin{bmatrix} - 2 \epsilon y_1  \\ - 2 \epsilon L_{fy_2} - \epsilon^2 y_2 \end{bmatrix}
\end{align}

\subsection{Partial Hybrid Zero Dynamics}
When the tracking outputs are driven to zero, the control law is expected to render the Hybrid Zero Dynamics (HZD) surface:
\begin{align}
	Z_{\alpha} = \{  (\theta, \dot{\theta}) \in TQ_R: y_1(\theta, \dot{\theta}) = 0, y_2(\theta, \alpha) = 0, L_{f}y_2(\theta, \dot{\theta}, \alpha) = 0 \}
\end{align}

This surface describes an invariant set for the continuous dynamics, once the tracking objectives are driven to zero, then the outputs will stay at zero for posterior times, however it is desired to guarantee that the outputs are impact invariant as well. The dependence of the surface with the parameter $\alpha$ allows the search of canonical walking functions that can render the surface hybrid invariant \cite{Westervelt}. Additionally, a less restrictive surface that allows changes in the hip velocity ($y^d_1 = v_{hip} $) after the impact, might help to compensate to impulsive effect of impacts and the modeling errors, this surface is the Partial Hybrid Zero Dynamics (PHZD).
\begin{align}
	PZ_{\alpha} = \{ (\theta, \dot{\theta}) \in TQ_R: y_2(\theta, \alpha) = 0, L_{f}y_2(\theta, \dot{\theta}, \alpha) = 0 \}
\end{align}

An impact on the system affects the joint velocities (\ref{eq:impact}), this can deviate the system evolution away from the PHZD. In order to constrain the system to be invariant under impacts it is necessary to ensure that when the system is in the switching surface $S$ evolving in the PHZD surface, an impact $\Delta$ does not move the system away from the PHZD surface $PZ_{\alpha}$. The PZHD constraint can be stated as:
\begin{align}
	\Delta(S \cap PZ_{\alpha}) = PZ_{\alpha}
	\label{eq:PHZDconstraint}
\end{align}

Therefore, the goal is to select the parameters $\alpha$ such that the system renders the PHZD invariant under impacts \cite{Westervelt,AmesFS}. This can be enforced as a constraint in the human-inspired optimization.


\section{Human-Inspired Optimization (HIO)}
The PHZD constraint generates human-like walking gaits that are provably stable \cite{AmesFS, Amber1,Amber2}. Additionally more constraints must be imposed considering the limitations of the prosthesis, such as torque, position and velocity limits. All the requirements of the walking gait must be specified in the optimization problem along with the physical limitations of the device itself. 
\begin{align}
\begin{split}
	\alpha^{*} = \underset{\alpha \in \mathbb{R}^{26}}{argmin} \hspace{1em} Cost_{HD}(\alpha) \\
	\Delta(S \cap PZ_{\alpha}) = PZ_{\alpha} \\
	(\text{Physical constraints})
\end{split}
\end{align}

The cost function $Cost_{HD}(\alpha)$ is the square weighted square error between human data and the CWF, as shown in the expression below, the complete details of the optimization can be found in \cite{AmesFS}. 
\begin{align}
	Cost_{HD}(\alpha) = \sum_{k=1}^K [(\beta_{y_1} ( y_1(t[k], v_{hip}) - y_1^H(k)))^2  + \sum_{j=1}^{5} \beta_{y_{2,j}}(y_{2,j}(t[k], \alpha) - y_{2,j}^H(k))^2 ]
\end{align}
where, $K$ is the total number of points taken during the motion capture, $y_1^H(k)$ is the vector of values corresponding to the hip position at index $k$. The relative degree two data is labeled as $y_{2,j}^H(k)$ beign the index $j$ the selector of one of the five relative degree two functions. The weights $\beta$ are defined as the difference between the maximum and the minimum value of the output data. 
\begin{align}
	\beta_{y_1} = 1 / (\max_k(y_{1}^H(k)) - \min_k(y_{1}^H(k))) \\
	\beta_{y_{2,j}} = 1 / (\max(y_{2,j}^H(k)) - \min(y_{2,j}^H(k))), \hspace{2em} j \in [1,5]
\end{align}

The solution of the human-inspired optimization is a walking gait that is provably stable and human-like due to the human data beign used. A Poincare analysis is carried out to guarantee stability ensuring that the eigenvalues are less than 1 \cite{WendelPoincare,PoincareBook, AmesFS}.
\newline
The strength of the human-inspired optimization is the automatic generation of stable and human-like walking gaits that requires no parameter tuning or calibration. Additionally, a series of constraints are imposed so the system can evolve in a feasible manner inside the designed bounds \cite{AMPRO1}. 

\section{Motion Capture System}
To acquire human data a IMU-based motion capture system was used. The IMUs consisted in seven MPU-9150 devices (tri-axis gyroscope and tri-axis accelerometer), they were placed in fixed places in the feet, calves, thighs and torso. A kinematic model is constructed in the sagital plane starting from the hip, with the assumption that the hip acceleration is negligible. During the data recording procedure, an EKF is utilized to estimate the joint trajectories, which will be used as the references for the prosthetic gait design. 
\newline
The human data captured by the IMU-based motion system is utilized on the human-inspired optimization (Fig. \ref{fig:cwf}), and particularly Fig. \ref{fig:MotionCaptureKneeAnkle} shows the ankle and knee joints. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=.25]{figures/CWF/matlab/MotionCaptureKneeAnkle.pdf}
\caption{Trajectories generated by the human-inspired optimization for ankle and knee.}
\label{fig:MotionCaptureKneeAnkle}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Note that the trajectories of the knee present differences with respect to normal human walking and it is because of the flat foot behavior requested to the subject. Solving the human-inspired optimization gives as a result the parameters $\alpha$ required to fully define the canonical walking functions. The ankle and knee trajectories generated for AMPRO II are shown in Fig. \ref{fig:KneeAnkleDesign}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=.25]{figures/CWF/matlab/KneeAnkleDesign.pdf}
\caption{Trajectories generated by the human-inspired optimization for ankle and knee.}
\label{fig:KneeAnkleDesign}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The phase portrait plot shown in Fig. \ref{fig:KneeAnkleOrbit} exhibits that the trajectories evolve into a continuous curve and then they are affected by a discrete impact (the discontinuity). Note that the discrete impact keeps the angle values but changes the joint velocities instantaneously (\ref{eq:impact}). 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=.25]{figures/CWF/matlab/AnkleKneeOrbits.pdf}
\caption{Ankle and knee phase portrait, indicating a continuous evolution of the system and a discrete change in velocity at impact.}
\label{fig:KneeAnkleOrbit}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Simulation of the Flat Ground Gait}
The simulation performed using feedback linearization generated an stable and human-like walking gait, represented in the walking tiles in Fig. \ref{fig:flatTile}. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=.25]{figures/CWF/matlab/simCWF1.pdf}
\includegraphics[scale=.25]{figures/CWF/matlab/simCWF2.pdf}
\caption{Canonical walking functions obtained in the simulation.}
\label{fig:simCWF}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The canonical walking functions generated for this particular walking gait are shown in Fig. \ref{fig:simCWF} for 4 steps. Note that hip velocity suffers a change after the impact but it converges to its desired value eventually. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[H]
\centering
\includegraphics[scale=.5]{figures/CWF/matlab/flatTile.pdf}
\caption{Gait tile simulation using feedback linearization control.}
\label{fig:flatTile}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Gait Generation for Non-Flat Terrain}
The human-inspired control can be used to generate walking gaits for diverse terrain. This section will summarize the application of this framework for rough terrain and for stair ascending, additionally it presents a discussion about the limitations of the current framework and possible options to address adaptability for transfemoral prosthesis. Note that the summary of the additional methods is rather informal but references are provided for more details.

\subsection{Extended Canonical Walking Functions (ECWF) for Rough Terrain}
Rough terrain imposes a challenge on the generation of human-inspired walking gaits due to the unknown and unpredictable terrain profile, Shishir \cite{RoughTerrain} proposed that one solution to handle this problem is the modification of the CWF into the extended canonical walking functions (ECWF). The ECWF are constructed by the addition of more parameters that can be calculated in real time, depending on the initial conditions imposed by the surface.
\begin{align}
	y^e_{2}(t, \alpha) = e^{-\alpha_4 t}(  \alpha_1 cos(\alpha_2 t) + \alpha_3 sin(\alpha_2 t)  ) + \alpha_5 cos(\alpha_6 t) + \frac{ 2 \alpha_5 \alpha_4 \alpha_2  }{  \alpha_2^2 + \alpha_4^2 - \alpha_6^2  } sin(\alpha_6 t) + \alpha_7
\end{align}
Choosing the linear terms $ \alpha_v = \begin{bmatrix} \alpha_1, \alpha_3, \alpha_5, \alpha_7 \end{bmatrix} $ we can construct a linear matrix equation by keeping $\alpha_2, \alpha_4, \alpha_6$ constant and solving the equation in order to accommodate the condition imposed by the surface \cite{RoughTerrain}. In this manner, it allows freedom in the terrain profile by calculating a new $ \alpha_v $ at every step. 

\subsection{Upstairs Walking}
The human-inspired optimization is highly dependent on the human-data and the terrain profile, using data for upstairs walking it would be possible to solve the human-inspired optimization given an appropriate redefinition of the guard $S$ (\ref{eq:guard}). In particular, the guard must define the terrain profile exactly in order to account for the impacts that happen during walking. Huihua et al. \cite{AMPROIstairs} generated a walking gait based on the human-inspired optimization given a priori knowledge of the stairs profile, the walking gait was implemented successfully into the prosthetic device AMPRO I.

\subsection{Limitations}
Note that in order to provide adaptability to new terrains it is important to generate joint trajectories that resemble the trajectories followed by healthy human subjects, for instance, while ECWF works for rough terrain, it cannot provide a specific desired behavior during upslope walking. Furthermore, if we are to use the human-inspired optimization for the generation of walking gaits at each new terrain, this problem could be unfeasible, as currently there is no solution on real time for this optimization. Additionally, in practical implementation with the prosthesis it is not possible to acquire information of the terrain profile.
\newline
In consequence, terrain adaptability requirements are twofold: i) Generation of trajectories with specific desired behaviors and ii) online solutions for the generation of new trajectories at every step. Regarding upslope walking, the generation of an algorithm that holds the two conditions for terrain adaptability takes the form of a convex optimization formulation. The reason for choosing a convex optimization problem is because it is fast to solve and can be subject to constraints and objectives that can be formulated to mimic the behavior of upslope walking. 
\newline
The next chapter will introduce the convex optimization problem that is aimed to the blending of the flat ground trajectories already generated into upslope walking by mimicking the qualitative strategies found in Chapter 2.













