
\renewcommand {\cftchapleader } {\cftdotfill {4.5}}
\contentsline {chapter}{ABSTRACT}{ii}{chapter*.1}
\contentsline {chapter}{DEDICATION}{iii}{chapter*.2}
\contentsline {chapter}{ACKNOWLEDGEMENTS}{iv}{chapter*.3}
\contentsline {chapter}{NOMENCLATURE}{v}{chapter*.4}
\contentsline {chapter}{TABLE OF CONTENTS}{vi}{section*.5}
\contentsline {chapter}{LIST OF FIGURES}{viii}{section*.6}
\contentsline {chapter}{LIST OF TABLES}{xi}{section*.7}
\contentsline {chapter}{\numberline {1}\uppercase {Introduction}}{1}{chapter.1}
\vskip +10pt
\contentsline {section}{\numberline {1.1}Upslope Walking}{6}{section.1.1}
\contentsline {section}{\numberline {1.2}Convex Optimization for Upslope Walking}{7}{section.1.2}
\contentsline {section}{\numberline {1.3}AMPRO II}{8}{section.1.3}
\contentsline {section}{\numberline {1.4}Thesis Structure}{10}{section.1.4}
\contentsline {chapter}{\numberline {2}\uppercase {Human upslope walking analysis}}{12}{chapter.2}
\vskip +10pt
\contentsline {section}{\numberline {2.1}Bipedal Locomotion}{12}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Kinematics of Human Walking}{12}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Upslope Walking Kinematics}{15}{section.2.2}
\contentsline {section}{\numberline {2.3}Human Walking Data}{15}{section.2.3}
\contentsline {section}{\numberline {2.4}Strategies for Walking Upslope}{17}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Human Strategy}{17}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Controller Strategy}{18}{subsection.2.4.2}
\contentsline {chapter}{\numberline {3}\uppercase {Flat terrain gait generation}}{20}{chapter.3}
\vskip +10pt
\contentsline {section}{\numberline {3.1}Bipedal Model}{20}{section.3.1}
\contentsline {section}{\numberline {3.2}Human-Inspired Control (HIC)}{24}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Canonical Walking Functions (CWF)}{24}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Human-Inspired Outputs (HIO)}{26}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Control Design}{27}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Partial Hybrid Zero Dynamics}{28}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Human-Inspired Optimization (HIO)}{29}{section.3.3}
\contentsline {section}{\numberline {3.4}Motion Capture System}{30}{section.3.4}
\contentsline {section}{\numberline {3.5}Simulation of the Flat Ground Gait}{33}{section.3.5}
\contentsline {section}{\numberline {3.6}Gait Generation for Non-Flat Terrain}{34}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Extended Canonical Walking Functions (ECWF) for Rough Terrain}{34}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Upstairs Walking}{35}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}Limitations}{35}{subsection.3.6.3}
\contentsline {chapter}{\numberline {4}\uppercase {Optimization-based Spline generation for upslope walking}}{37}{chapter.4}
\vskip +10pt
\contentsline {section}{\numberline {4.1}Problem Formulation}{37}{section.4.1}
\contentsline {section}{\numberline {4.2}Analytical Solution}{43}{section.4.2}
\contentsline {section}{\numberline {4.3}Human-Inspired Walking with Cubic Splines}{44}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Generated Trajectories}{46}{subsection.4.3.1}
\contentsline {section}{\numberline {4.4}Implementation Details}{46}{section.4.4}
\contentsline {chapter}{\numberline {5}\uppercase {Results}}{48}{chapter.5}
\vskip +10pt
\contentsline {section}{\numberline {5.1}Flat Ground Walking}{49}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}HIC Results}{50}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}SHIC Results}{51}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Upslope Walking}{51}{section.5.2}
\contentsline {section}{\numberline {5.3}Controllers Comparison}{51}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Flat Ground Walking using HIC and SHIC}{52}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Flat Ground and Upslope Walking using SHIC}{53}{subsection.5.3.2}
\contentsline {section}{\numberline {5.4}SHIC Control Action}{53}{section.5.4}
\contentsline {chapter}{\numberline {6}\uppercase {CONCLUSIONS}}{58}{chapter.6}
\contentsline {chapter}{REFERENCES}{59}{section*.8}
