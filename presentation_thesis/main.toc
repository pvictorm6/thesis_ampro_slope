\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Problem description}{2}{0}{1}
\beamer@subsectionintoc {1}{2}{Tranfemoral prosthesis}{5}{0}{1}
\beamer@subsectionintoc {1}{3}{Solution}{7}{0}{1}
\beamer@sectionintoc {2}{Human-Inspired control}{11}{0}{2}
\beamer@subsectionintoc {2}{1}{Model construction}{11}{0}{2}
\beamer@subsectionintoc {2}{2}{Tracking functions}{14}{0}{2}
\beamer@subsectionintoc {2}{3}{Stability}{21}{0}{2}
\beamer@sectionintoc {3}{Spline generation}{25}{0}{3}
\beamer@subsectionintoc {3}{1}{Human Strategies}{25}{0}{3}
\beamer@subsectionintoc {3}{2}{Spline construction}{27}{0}{3}
\beamer@subsectionintoc {3}{3}{Results}{36}{0}{3}
